# coding: utf-8
# frozen_string_literal: true
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'roda/endpoints/version'

# rubocop:disable Metrics/BlockLength
Gem::Specification.new do |spec|
  spec.name = 'roda-endpoints'
  spec.version = Roda::Endpoints::VERSION
  spec.authors = ['Alex Semyonov']
  spec.email = ['alex@semyonov.us']

  spec.summary = 'RESTful endpoints for Roda tree'
  spec.description = 'Generic endpoints and specific implementations.'
  spec.homepage = 'http://alsemyonov.gitlab.com/roda-endpoints/'
  spec.license = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'dry-container'
  spec.add_runtime_dependency 'dry-matcher'
  spec.add_runtime_dependency 'dry-transaction'
  spec.add_runtime_dependency 'dry-types'
  spec.add_runtime_dependency 'dry-validation'
  spec.add_runtime_dependency 'roda', '~> 2.21.0'
  spec.add_runtime_dependency 'roda-flow', '~> 0.3.0'
  spec.add_runtime_dependency 'roda-monads', '~> 0.1.0'
  spec.add_runtime_dependency 'rom-repository'

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'bundler-audit', '~> 0.5.0'
  spec.add_development_dependency 'rack-test', '~> 0.6.3'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec-roda', '~> 0.1.0'
  spec.add_development_dependency 'rubocop', '~> 0.47.0'
  spec.add_development_dependency 'simplecov', '~> 0.12.0'
  spec.add_development_dependency 'yard', '~> 0.9.5'
  spec.add_development_dependency 'rom-repository', '>= 0.3.1'
  spec.add_development_dependency 'rom-sql', '>= 0.9'
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency 'pry-byebug'
end
# rubocop:enable Metrics/BlockLength
