# frozen_string_literal: true

require 'roda/endpoints/endpoint/data/repository'

module Repositories
  # Users repository
  class Users < ROM::Repository[:users]
    include Roda::Endpoints::Endpoint::Data::Repository

    commands :create, update: :by_pk, delete: :by_pk
  end
end
