# frozen_string_literal: true
$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require 'simplecov'
require 'rspec/roda'
require 'roda/endpoints'
require 'rom'
require 'rom/repository'
require 'pp'
require 'pry-byebug'

RSpec.shared_context 'endpoints', name: :endpoints do
  include_context 'Roda plugin', roda: :plugin, name: :endpoints
  roda { plugin :endpoints }

  before do
    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
  end

  Roda::Endpoints::VERBS.each do |verb|
    define_method(verb) do |uri, params = {}, env = {}|
      super(uri, {}, env.merge(
        method: verb.upcase,
        params: {},
        Rack::RACK_INPUT => StringIO.new(JSON.generate(params))
      ))
      # env = env_for(
      #   uri,
      #   env.merge(
      #     method: verb.upcase,
      #     params: {},
      #     Rack::RACK_INPUT => StringIO.new(JSON.generate(params))
      #   )
      # )
      # process_request(uri, env, &block)
    end
  end


  # @return [Hash]
  def endpoint_metadata
    self.class.metadata[:endpoint].to_hash || {}
  end

  let(:name) { endpoint_metadata[:name] || :endpoint }
  let(:attributes) do
    endpoint_metadata.each_with_object(
      name: name,
      container: roda_class
    ) do |(field, value), attributes|
      attributes[field] = value
    end
  end

  let(:rom) do
    ROM.container(:sql, 'sqlite::memory') do |config|
      connection = config.default.connection
      connection.create_table(:users) do
        primary_key :id
        column :email, String, null: false
        column :created_at, Time,
               null: false,
               default: Sequel::CURRENT_TIMESTAMP
        column :updated_at, Time,
               null: false,
               default: Sequel::CURRENT_TIMESTAMP
      end

      connection.create_table(:articles) do
        primary_key :id
        column :title, String, null: false
        column :content, String, null: false
        column :created_at, Time,
               null: false,
               default: Sequel::CURRENT_TIMESTAMP
        column :updated_at, Time,
               null: false,
               default: Sequel::CURRENT_TIMESTAMP
        foreign_key :user_id, :users, type: Integer, null: false
      end

      config.relation(:users) do
        schema(infer: true) do
          associations do
            one_to_many :articles
          end
        end
      end

      config.relation(:articles) do
        schema(infer: true) do
          associations do
            belongs_to :user
          end
        end
      end
    end
  end

  let(:users) do
    require 'repositories/users'
    Repositories::Users.new(rom)
  end

  let(:articles) do
    require 'repositories/articles'
    Repositories::Articles.new(rom)
  end

  before do
    user = users.create(email: 'example@example.com')

    expect(articles.list).to eq []
    articles.create(title: 'Title', content: 'Content', user_id: user.id)
    expect(articles.list).to match [kind_of(ROM::Struct)]

    roda_class.register('repositories.users', users)
    roda_class.register('repositories.articles', articles)
  end
end

RSpec.shared_context 'endpoint', :endpoint do
  include_context 'endpoints'
  name = metadata[:name]

  subject(:endpoint) { described_class.new(**attributes) }

  it { is_expected.to be_a Roda::Endpoints::Endpoint }
end
