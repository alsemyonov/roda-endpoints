# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Roda::Endpoints::Endpoint::Collection,
               '(name: :articles)',
               endpoint: { name: :articles } do
  its(:name) { is_expected.to eq :articles }
  specify { expect(described_class.verbs).to eq Set.new %i(get post) }
end
