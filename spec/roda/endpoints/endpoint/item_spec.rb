# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Roda::Endpoints::Endpoint::Item, endpoint: { name: :article } do
  let(:id) { :articles }
  it 'supports default set of verbs' do
    expect(described_class.verbs).to eq Set.new %i(get patch put delete)
  end
end
