# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Roda::Endpoints::Endpoint, '(name: :endpoint)',
               endpoint: { name: :endpoint } do
  its(:name) { is_expected.to eq :endpoint }
end
