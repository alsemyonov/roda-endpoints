# frozen_string_literal: true

require 'spec_helper'
require 'rspec/mocks/standalone'
RSpec::Mocks::Syntax.enable_expect

RSpec.describe Roda::RodaPlugins::Endpoints, roda: :plugin, name: :endpoints do
  route do |r|
    # Assume we are logged in as user with ID == 1
    user_id = 1

    r.singleton :user, id: user_id do |account|
      expect(account.repository_key).to eq 'repositories.users'
      account.validate :patch do
        required(:user).schema do
          required(:email, :string).filled(min_size?: 5)
        end
      end
    end

    # @route /articles
    # expect(r).to receive(:on).with('articles').and_call_original
    r.collection :articles do |articles|
      expect(articles.repository_key).to eq 'repositories.articles'
      # articles.defaults articles.resource_name => { user_id: user_id }

      if r.post?
        r.params[articles.item_name] ||= {}
        r.params[articles.item_name][:user_id] = user_id
      end

      # byebug
      expect(articles).to be_a(Roda::Endpoints::Endpoint::Collection)

      expect(articles).to respond_to :validate
      articles.validate :post do
        required(:article).schema do
          required(:title, :string).filled(min_size?: 1)
          required(:content, :string).filled(min_size?: 1)
          required(:user_id).filled
        end
      end

      expect(articles).to respond_to :step
      expect(articles).to respond_to :after
      expect(articles).to respond_to :before
      # articles.before :persist, only: :post do |params|
      #   params[resource_name][:user_id] = user_id
      #   Right(params)
      # end

      expect(articles.verbs).to eq %i(get post)

      # expect(r).to(
      #   receive(:on).with(kind_of(Hash))
      #     .at_least(1).times
      #     .and_call_original
      # )
      # expect(r).to receive(:on).with(:id).at_least(1).times.and_call_original
      # @route /articles/{id}
      r.item on: :id do |article|
        expect(article).to be_a(Roda::Endpoints::Endpoint::Item)
      end

      # if r.is_get?
      #   list = articles.perform(:get, {})
      #   list = list.value
      #   expect(list.size).to eq 1
      #   expect(list).to match [kind_of(ROM::Struct)]
      #   # expect(articles).to receive(:call).with(:get, {}).and_call_original
      # end
      #
      if r.post?
        # article = articles.perform(:post, {
        #   'article' => {
        #     'title' => 'Title',
        #     'content' => 'Content',
        #     'user_id' => 1
        #   }
        # })
        # article = article.value
        # expect(article.to_hash).to(
        #   match(
        #     id: 2,
        #     title: 'Title',
        #     content: 'Content',
        #     user_id: 1
        #   )
        # )
        # expect(articles).to(
        #   receive(:perform).with(:post, kind_of(Hash)).and_call_original
        # )
      end

      # if r.params[:id]
      #   expect(r).to receive(:on).with(:id).and_call_original
      # end
      # expect(articles).to receive(:call).with(:get, {}).and_call_original
    end
    # r.resource :user, id: proc { |r| r.session[:user_id] } do |user|
    #   expect(user).to be_a(Roda::Endpoints::Endpoint::Resource)
    # end
  end

  describe 'app' do
    subject { last_response }
    let(:json) do
      expect(last_response.body).not_to be_empty
      JSON.parse(last_response.body, symbolize_names: true)
    end

    describe 'singleton' do
      describe :get do
        before { get '/user' }

        its(:status) { is_expected.to eq 200 }
        it 'responds with correct JSON' do
          expect(json).to(
            match(
              id: 1,
              email: 'example@example.com',
              created_at: kind_of(String),
              updated_at: kind_of(String)
            )
          )
        end
      end

      describe :patch do
        context 'with valid attributes' do
          before do
            patch '/user', user: { email: 'example@example.org' }
          end

          its(:status) { is_expected.to eq 202 }
          it 'responds with correct JSON' do
            expect(json).to(
              match(
                id: 1,
                email: 'example@example.org',
                created_at: kind_of(String),
                updated_at: kind_of(String)
              )
            )
          end
        end
      end
    end

    describe 'collection' do
      describe :get do
        before { get '/articles' }

        its(:status) { is_expected.to eq 200 }
        it do
          expect(json).to match_array [{
            id: 1,
            title: 'Title',
            content: 'Content',
            user_id: 1,
            created_at: kind_of(String),
            updated_at: kind_of(String)
          }]
        end
      end

      describe :post do
        context 'with valid attributes' do
          before do
            post '/articles', article: { title: 'Title', content: 'Content' }
          end

          its(:status) { is_expected.to eq 201 }
          it 'responds with correct JSON' do
            expect(json).to match(
              id: 2,
              title: 'Title',
              content: 'Content',
              user_id: 1,
              created_at: kind_of(String),
              updated_at: kind_of(String)
            )
          end
        end
      end
    end

    describe 'item' do
      describe :get do
        before { get '/articles/1' }

        its(:status) { is_expected.to eq 200 }
        it 'responds with correct JSON' do
          expect(json).to match(
            id: 1,
            title: 'Title',
            content: 'Content',
            user_id: 1,
            created_at: kind_of(String),
            updated_at: kind_of(String)
          )
        end
      end

      xdescribe :patch do
        context 'with valid attributes' do
          before do
            patch '/articles/1', article: { title: 'New Title' }
          end

          its(:status) { is_expected.to eq 202 }
          it 'responds with correct JSON' do
            expect(json).to match(
              id: 2,
              title: 'New Title',
              content: 'Content',
              user_id: 1
            )
          end
        end
      end

      xdescribe :put do
        context 'with valid attributes' do
          before do
            put '/articles/1', article: { title: 'New Title' }
          end

          its(:status) { is_expected.to eq 202 }
          it 'responds with correct JSON' do
            expect(json).to match(
              id: 2,
              title: 'New Title',
              content: 'Content',
              user_id: 1
            )
          end
        end
      end

      describe :delete do
        context 'with valid attributes' do
          before do
            delete '/articles/1'
          end

          its(:status) { is_expected.to eq 204 }
          its(:body) { is_expected.to eq '' }
        end
      end
    end
  end
end
