# frozen_string_literal: true
require 'rom'
require 'rom/struct'

module ROM
  # Simple data-struct
  #
  # By default mappers use this as the model
  #
  # @api public
  class Struct
    alias as_json to_hash

    def to_json(*args)
      as_json.to_json(*args)
    end
  end
end
