# frozen_string_literal: true

require 'transproc'
require 'roda/endpoints'

class Roda
  # Useful transformations.
  module Functions
    extend Transproc::Registry

    import Transproc::HashTransformations

    # Shortcut f
    module Shortcut
      # @param [Symbol] fn
      # @param [Array] args
      def f(fn, *args)
        if args.any?
          Functions[fn].call(*args)
        else
          Functions[fn]
        end
      end
    end
  end
end
