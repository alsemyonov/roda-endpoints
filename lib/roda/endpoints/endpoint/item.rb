# frozen_string_literal: true

require 'roda/endpoints/endpoint/singleton'
require 'forwardable'

class Roda
  module Endpoints
    class Endpoint
      # HTTP endpoint representing a specific item of collection uniquely
      # identified by some parameter.
      class Item < Singleton
        extend Forwardable

        # @return [Endpoint::Collection]
        def collection
          parent
        end

        # @!method id
        #   @return [Integer]
        id_proc do
          instance_variable_defined?(:@id) ? @id : captures.last
        end

        def_delegator :collection, :repository
      end
    end
  end
end
