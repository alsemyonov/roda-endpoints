# frozen_string_literal: true

require 'roda/endpoints/endpoint'
require 'roda/endpoints/endpoint/property'
require 'dry-initializer'
require 'inflecto'

class Roda
  module Endpoints
    class Endpoint
      # {Properties} container of {Endpoint}.
      module Properties
        # Class-level interface for {Endpoint endpoint’s}
        # {Properties properties}.
        module ClassInterface
          def properties
            @properties ||= {}
          end

          # @return [Property]
          def property(name, **kwargs)
            properties[name] = Property.new(name: name, **kwargs)
          end
        end

        def self.included(child)
          child.module_eval do
            extend ClassInterface, Macros

            child.properties = properties.dup
            property :properties, inherit: true
          end
          super
        end
      end
    end
  end
end
