# frozen_string_literal: true

class Roda
  module Endpoints
    # Generic HTTP endpoint abstraction.
    class Endpoint
      # Accessing data inside of endpoint.
      module Caching
        # @param last_modified [Symbol]
        # @param attributes [{Symbol=>Object}]
        def initialize(last_modified: Undefined, **attributes)
          @last_modified = last_modified unless last_modified == Undefined
          super(**attributes)
        end

        # @return [Time]
        def last_modified
          Time.now
        end

        # @return [{Symbol=>Object}]
        def to_hash
          @last_modified ? super.merge(last_modified: @last_modified) : super
        end
      end
    end
  end
end
