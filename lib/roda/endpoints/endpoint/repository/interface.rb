# frozen_string_literal: true

require 'roda/endpoints'

class Roda
  module Endpoints
    class Endpoint
      module Repository
        # {Endpoints}-compatible interface for `ROM::Repository`.
        module Interface
          # Class-level interface for `ROM::Repository`
          module ClassInterface
            # @return [{Symbol=>Symbol}]
            def filters
              @filters ||= {}
            end

            # @return [Symbol]
            # @param [Symbol] name filter name (query parameter)
            # @param [Symbol] by column name
            def filter(name, by:)
              filters[name] = { by: by }
              define_method name do |value|
                where(by => value)
              end
            end
          end

          def self.included(child)
            child.module_eval do
              extend ClassInterface
            end
            super
          end

          def filter(scope: root, **kwargs)
            self.class.filters.each do |filter|
              scope = scope.public_send(filter, kwargs[filter]) if kwargs.key?(filter)
            end
            scope
          end

          # @param [<ROM::Struct>] kwargs
          def list(**kwargs)
            filter(**kwargs).to_a
          end


          # @return [Time]
          def last_modified
            root.order(Sequel.desc(:updated_at)).first&.updated_at
          end

          # @param [Integer] id
          # @return [ROM::Struct]
          def find(id)
            if id.to_i < 1
              raise ArgumentError,
                    "#{self.class}#fetch: invalid id provided: #{id.inspect}"
            end
            root.by_pk(id.to_i).one!
          end
        end
      end
    end
  end
end
