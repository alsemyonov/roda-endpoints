# frozen_string_literal: true

require 'roda/endpoints/endpoint'
require 'roda/endpoints/types'
require 'inflecto'
require 'forwardable'

class Roda
  module Endpoints
    class Endpoint
      # Attribute macros.
      module Macros
        include Forwardable

        def self.append_features(child)
          super
          child.module_eval do
            extend child.class_extension
            include child.instance_interface
          end
        end

        def enable_macros!
          extend class_interface
          include instance_interface
        end

        # @param [Property] property
        # @param [#call] block
        def define_property_accessor(property, &block)
          block ||= proc do |value = Undefined|
            public_send(property.writer, value) if value != Undefined
            instance_variable_get(property.ivar)
          end
          class_interface { define_method(property.reader, &block) }
        end

        # @param [Property] property
        # @param [#call] block
        def define_property_writer(property, &block)
          block ||= proc do |value|
            instance_variable_set(property.ivar, value)
          end
          class_interface { define_method(property.writer, &block) }
        end

        def define_property_merger(property, &block)
          return unless property.bang
          block ||= proc do |value|
            public_send(
              property.writer,
              (public_send(name) || property.default.call).public_send(
                property.merger, value
              )
            )
          end
          class_interface { define_method(property.bang, &block) }
        end

        def define_property_writer(property)
          return unless property.writer
          class_interface do
            return if method_defined?(property_writer)
            define_method property.writer do |value|
              instance_variable_set(
                property.ivar,
                property.freeze ? value.freeze : value
              )
            end
          end
        end

        def define_instance_reader(property)
          return unless property.instance
          instance_interface do
            define_method(property.instance) do |*args|
              self.class.public_send(property.name, *args)
            end
          end
        end

        def define_class_aliases(*aliases)
          def_instance_delegators 'self.class', *aliases.flatten
        end

        # @param [Property] property
        def class_property(property)
          class_interface do
            if property.default
              define_method property.name do
                if instance_variable_defined?(property.ivar)
                  instance_variable_get(property.ivar)
                else
                  instance_variable_set(
                    property.ivar,
                    instance_exec(&property.default)
                  )
                end
              end
            else
              define_method property.name do
                instance_variable_get(property.ivar)
              end
            end
            inherit_class_property property.name if property.inherit
          end
        end

        def inherit_class_property(property)
          define_property_writer(property)
          class_interface do
            attr_writer property.name unless method_defined?(property.writer)
          end
          self.properies = properties.merge(name => property).freeze
        end

        def inherited_properties
          @inherited_properties ||= Set.new
        end

        def inherited(child)
          child.attributes.each do |name, inherited:, **_attribute|
            child.public_send(name, public_send(name)) if inherited
          end
          super
        end

        # @param [#call] block
        def class_interface(&block)
          interface(:ClassInterface, &block)
        end

        # @param [Symbol] name
        # @param [#call] block
        def interface(name, &block)
          interface = const_set(name, Module.new) unless const_defined?(name)
          interface ||= const_get(name)
          interface.module_eval(&block) if block_given?
          interface
        end

        # @return [Module]
        def instance_interface(&block)
          interface(:InstanceInterface, &block)
        end
      end
    end
  end
end
