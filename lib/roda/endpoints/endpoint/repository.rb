# frozen_string_literal: true

require 'roda/endpoints/endpoint'
require 'inflecto'

class Roda
  module Endpoints
    class Endpoint
      # Accessing data inside of endpoint.
      module Data
        def self.included(child)
          child.module_eval do
            attribute :repository_key,
                      type: Types::Strict::String,
                      default: (proc do
                        "repositories.#{Inflecto.pluralize(name.to_s)}"
                      end)
            attribute :repository,
                      default: proc { container[repository_key] },
                      inject: true
          end
        end
      end
    end
  end
end
