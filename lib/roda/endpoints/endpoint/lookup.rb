# frozen_string_literal: true

require 'roda/endpoints/endpoint/namespace'

class Roda
  module Endpoints
    class Endpoint
      # Lookup everything in the container.
      module Lookup
        prepend Namespace

        # @return [Dry::Container::Mixin]
        def container
          @container || parent&.container || Roda::Endpoints.roda_class
        end

        # @param [<String>] paths
        def lookup(*paths, key: nil, scope: nil, default: nil)
          if key && scope
            paths += lookup_keys(key: key, scope: scope, default: default)
          end
          paths.flatten.detect { |full_key| container.key?(full_key) }
        end

        # @param [#to_s] key
        # @param [#to_s] scope
        def lookup_keys(key:, scope: nil, default: nil, lookup: lookup_path)
          scope = "#{scope}." unless scope.to_s.end_with?('.')
          keys = []
          Array(key).flatten.map do |sect|
            lookup.each do |choice|
              [sect, default].compact.each do |value|
                full_key = "#{scope}#{choice}.#{value}"
                yield full_key if block_given?
                keys << full_key
              end
            end
          end
          keys
        end

        def lookup_path
          path = [ns]
          endpoint = self.class
          while endpoint < Roda::Endpoints::Endpoint
            path << endpoint.type
            endpoint = endpoint.superclass
          end
          path
        end

        # @param [Symbol] verb
        # @return [String]
        def operation_for(verb)
          lookup key: verb, scope: 'operations.'
        end

        # @param [Symbol] verb
        # @return [String]
        def validation_for(verb)
          lookup(key: verb, default: 'any', scope: 'validations.') ||
            provide_default_validation!
        end

        # @param [Symbol] verb
        # @return [String]
        def transaction_for(verb)
          lookup key: verb, scope: 'transactions'
        end
      end
    end
  end
end
