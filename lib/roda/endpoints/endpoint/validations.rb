# frozen_string_literal: true

require 'roda/endpoints'
require 'roda/endpoints/functions'
require 'dry-validation'

class Roda
  module Endpoints
    # Generic HTTP endpoint abstraction.
    class Endpoint
      # Parameters validations for {Endpoints} based on `Dry::Validation` gem.
      module Validations
        include Functions::Shortcut

        def defaults(verb = :any, **attributes)
          key = "validations.defaults.#{ns}.#{verb}" if key.is_a?(Symbol)
          container.register key, attributes
        end

        # @param [String, Symbol] verb
        # @param [Proc] block
        #
        # @example
        #   r.collection :articles do |articles|
        #     # register validation at 'validations.endpoints.articles.default'
        #     articles.validate do
        #       required(:title).filled?
        #       required(:contents).filled?
        #     end
        #     # redefine validation for patch method at
        #     # 'validations.endpoints.articles.patch'
        #     articles.validate(:patch) do
        #       required(:title).filled?
        #       required(:contents).filled?
        #       require(:confirm_changes).
        #     end
        #   end
        def validate(verb = :any, &block)
          defaults = "validations.defaults.#{ns}.#{verb}"
          verb = "validations.#{ns}.#{verb}"
          schema = Dry::Validation.Form(&block)
          container.register(verb) do |params|
            if container.key?(defaults)
              params = f(:deep_merge).call(params, container[defaults])
            end
            validation = schema.call(params)
            if validation.success?
              Right(validation.output)
            else
              Left([:unprocessable_entity, {}, validation])
            end
          end
          schema
        end

        # rubocop:enable Metrics/MethodLength

        # @param [Symbol] verb
        # @return [Dry::Validation::Schema::Form]
        def validation(verb)
          if (validation = validation_for(verb))
            container[validation]
          else
            # default validation requires no params and provide no results
            provide_default_validation!
          end
        end

        def provide_default_validation!
          validate {} unless container.key?("validations.#{ns}.any")
        end

        def prepare_validations!
          return if @validations_prepared
          (verbs - %i(get head options)).each do |verb|
            key = "validations.#{ns}.#{verb}"
            default = "validations.#{ns}.any"
            unless container.key?(key) || container.key?(default)
              provide_default_validation!
            end
          end
          @validations_prepared = true
        end
      end
    end
  end
end
