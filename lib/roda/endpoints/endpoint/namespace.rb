# frozen_string_literal: true

class Roda
  module Endpoints
    # Generic HTTP endpoint abstraction.
    class Endpoint
      # Namespacing operations, validations, etc.
      module Namespace
        def self.included(child)
          child.attributes += %i(name ns parent)
        end

        # @param name [Symbol]
        # @param ns [String]
        # @param attributes [{Symbol=>Object}]
        # @param parent [Endpoint?]
        def initialize(name:, ns: name.to_s, parent: Undefined, **attributes)
          @name = name
          @ns = ns
          unless parent == Undefined
            @parent = parent
            @ns = [parent.ns, ns].compact.join('.')
          end
          super(name: name, **attributes)
        end

        # @return [Symbol]
        attr_reader :name

        # @return [String]
        attr_reader :ns

        # @return [Endpoint]
        attr_reader :parent
      end
    end
  end
end
