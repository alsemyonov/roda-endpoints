# frozen_string_literal: true

require 'roda/endpoints/endpoint'
require 'dry-initializer'
require 'inflecto'

class Roda
  module Endpoints
    class Endpoint
      # Properties of {Endpoint}s.
      class Property
        extend Dry::Initializer::Mixin

        option :name, Types::Strict::Symbol
        option :type,
               default: proc { nil }
        option :default,
               default: proc { Undefined }
        option :ivar, Types::Strict::Symbol,
               default: proc { :"@#{name}" }
        option :reader, Types::Strict::Symbol,
               default: proc { name.to_sym }
        option :writer, Types::Strict::Symbol,
               default: proc { :"#{name}=" }
        option :bang, Types::Strict::Symbol,
               default: proc { :"#{name}!" }
        option :instance, Types::Strict::Symbol,
               default: proc { name.to_sym }
        option :merger, Types::Strict::Symbol,
               default: proc { :+ }
        option :inherited, Types::Strict::Bool,
               default: proc { true }
        option :freeze, Types::Strict::Bool,
               default: proc { true }
        option :inject, Types::Strict::Bool,
               default: proc { false }

        attr_accessor :defined

        def attribute_properties
          properties = to_hash
          if default
            properties[:default] = default
          else
            properties.delete(:default) { nil }
          end
          properties
        end
      end
    end
  end
end
