# frozen_string_literal: true

require 'roda/endpoints'
require 'dry-types'

class Roda
  module Endpoints
    # Generic HTTP endpoint abstraction.
    module Types
      include Dry::Types.module
    end
  end
end
