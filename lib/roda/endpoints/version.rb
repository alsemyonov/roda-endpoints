# frozen_string_literal: true

class Roda
  module Endpoints
    VERSION = '0.3.7'
  end
end
