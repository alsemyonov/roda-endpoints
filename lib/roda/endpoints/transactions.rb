# frozen_string_literal: true

require 'roda/endpoints'
require 'dry-configurable'
require 'dry-transaction'
require 'forwardable'

class Roda
  module Endpoints
    # The DSL for defining {Transactions transactions} for endpoints.
    class Transactions
      extend Dry::Configurable
      extend Forwardable

      setting :container
      setting :options, {}

      def self.define
        yield new
      end

      # @return [{Symbol=>Object}]
      def self.options
        { container: config.container }.merge(config.options)
      end

      # @param [Endpoint] endpoint
      # @param [Hash] options
      def initialize(endpoint:, **options)
        @endpoint = endpoint
        @container = endpoint.container
        @options = self.class.options
                       .merge(container: endpoint.container)
                       .merge(options)
      end

      # @return [{Symbol=>Object}]
      attr_reader :options

      # @return [Endpoint]
      attr_reader :endpoint

      # @return [Dry::Container::Mixin]
      attr_reader :container

      def_delegators :endpoint, :operation_for, :validation_for

      # @param [Symbol] shortcut
      # @param [Proc] block
      def define(shortcut, &block)
        key = key_for(shortcut)
        return if container.key? key
        container.register(
          key,
          Dry.Transaction(
            container: container,
            endpoint: endpoint,
            &block
          )
        )
      end

      # @param [Symbol] verb
      # @return [String]
      def key_for(verb)
        "transactions.#{endpoint.ns}.#{verb}"
      end

      # @param [Symbol] shortcut
      def [](shortcut)
        key = key_for(shortcut)
        container[key]
      end
    end
  end
end
