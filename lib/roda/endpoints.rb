# frozen_string_literal: true
require 'roda/endpoints/version'
require 'dry/core/constants'
require 'dry/container'

class Roda
  # Endpoints abstractions
  module Endpoints
    include Dry::Core::Constants

    autoload :Endpoint, 'roda/endpoints/endpoint'

    VERBS = %i(get post delete patch put head).freeze

    # @return [Dry::Container]
    def self.container
      @container ||= Dry::Container.new
    end

    class << self
      attr_accessor :roda_class
    end
  end
end

require 'roda/plugins/endpoints'
